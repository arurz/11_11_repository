﻿namespace ExamProjectApi.Enums
{
    public enum ApplicationStatus
    {
        InCreation,
        InProcessing,
        Approved,
        Disapproved
    }
}
