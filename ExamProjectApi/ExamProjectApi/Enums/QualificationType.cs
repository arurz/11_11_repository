﻿namespace ExamProjectApi.Enums
{
    public enum QualificationType
    {
        Intership,
        QualificationCourse
    }
}
