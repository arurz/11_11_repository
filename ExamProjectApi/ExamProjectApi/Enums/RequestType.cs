﻿namespace ExamProjectApi.Enums
{
    public enum RequestType
    {
        Exercising,
        CertificateObtaining
    }
}
