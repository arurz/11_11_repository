﻿using ExamProjectApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ExamProjectApi.Persistence
{
    public class ProjectDbContext : DbContext
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }

        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options)
        {   }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationConfiguration).Assembly);
        }
    }
}
