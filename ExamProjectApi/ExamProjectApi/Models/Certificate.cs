﻿
namespace ExamProjectApi.Models
{
    public class Certificate
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Municipality { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public int DurationIntershipDays { get; set; }
        public int DurationCourseDays { get; set; }

        public string Status { get; set; }
    }
}
