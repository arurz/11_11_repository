﻿using ExamProjectApi.Enums;
using System;

namespace ExamProjectApi.Models
{
    public class Qualification
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinalDate { get; set; }
        public int DurationDays { get; set; }
        public string ShortDescription { get; set; }

        private QualificationType QualificationType;
        public QualificationType Type
        {
            get
            {
                return QualificationType;
            }
            set
            {
                QualificationType = value;
            }
        }

        public int ApplicationId { get; set; }
        public Application Application { get; set; }
    }
}
