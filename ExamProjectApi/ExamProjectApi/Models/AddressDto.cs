﻿namespace ExamProjectApi.Models
{
    public class AddressDto
    {
        public int Id { get; set; }
        public string Municipality { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string ApplicantAddress { get; set; }
    }
}
