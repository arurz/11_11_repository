﻿using ExamProjectApi.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace ExamProjectApi.Models
{
    public class Application
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public Address Address { get; set; }

        private ApplicationStatus ApplicationStatus;
        public ApplicationStatus Status
        {
            get
            {
                return ApplicationStatus;
            }
            set
            {
                ApplicationStatus = value;
            }
        }

        private RequestType requestType;
        public RequestType RequestType
        {
            get
            {
                return requestType;
            }
            set
            { 
                requestType = value;
            }
        }

        public ICollection<Qualification> Qualifications { get; set; } = new List<Qualification>();
    }
    public class ApplicationConfiguration : IEntityTypeConfiguration<Application>
    {
        public void Configure(EntityTypeBuilder<Application> builder)
        {
            builder.Property(b => b.FirstName).IsRequired();
            builder.Property(b => b.LastName).IsRequired();

            builder.HasOne<Address>(ap => ap.Address)
                .WithOne(ad => ad.Application)
                .HasForeignKey<Address>(ap => ap.AplicationId);
        }
    }
}
