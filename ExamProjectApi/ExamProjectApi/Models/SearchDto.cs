﻿using ExamProjectApi.Enums;

namespace ExamProjectApi.Models
{
    public class SearchDto
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Municipality { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public int Status { get; set; }
    }
}
