﻿using ExamProjectApi.Models;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ExamProjectApi.Services
{
    public class EmailService
    {

        public async Task SendEmail(Application application)
        {
            var toAddress = new MailAddress(application.Email);
            var fromAddress = new MailAddress("ggamestore1@gmail.com");
            MailMessage message = new(fromAddress, toAddress);

            message.Subject = "Status change";
            message.Body = $"{application.FirstName} {application.LastName}, your request status has been changed to {application.Status}";
            message.BodyEncoding = Encoding.UTF8;
            message.IsBodyHtml = true;

            SmtpClient client = new("smtp.gmail.com", 587);
            NetworkCredential basicCredemtial = new(fromAddress.Address, "GameStore126578934ProjectMaiL");

            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredemtial;

            try
            {
                client.Send(message);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
