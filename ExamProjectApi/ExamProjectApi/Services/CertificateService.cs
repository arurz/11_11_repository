﻿using ExamProjectApi.Enums;
using ExamProjectApi.Interfaces;
using ExamProjectApi.Models;
using ExamProjectApi.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamProjectApi.Services
{
    public class CertificateService : ICertificateService
    {
        private readonly ProjectDbContext context;
        public CertificateService(ProjectDbContext context)
        {
            this.context = context;
        }

        public async Task<Certificate> GetCertificateById(int id)
        {
            var application = await context.Applications
                .Include(app => app.Address)
                .Include(app => app.Qualifications)
                .SingleOrDefaultAsync(app => app.Id == id);

            var durationIntershipDays = 0;
            var durationCourseDays = 0;

            foreach (var qualification in application.Qualifications)
            {
                if (qualification.Type == QualificationType.Intership)
                {
                    durationIntershipDays += qualification.DurationDays;
                }

                if (qualification.Type == QualificationType.QualificationCourse)
                {
                    durationCourseDays += qualification.DurationDays;
                }
            }

            var certificate = new Certificate()
            {
                Id = application.Id,
                FirstName = application.FirstName,
                MiddleName = application.MiddleName,
                LastName = application.LastName,

                Municipality = application.Address.Municipality,
                Region = application.Address.Region,
                City = application.Address.City,

                DurationCourseDays = durationCourseDays,
                DurationIntershipDays = durationIntershipDays,
                Status = Enum.GetName(typeof(ApplicationStatus), application.Status)
            };

            return certificate;
        }

        public async Task<List<Certificate>> GetCertificates(List<Application> applications)
        {
            var certificates = new List<Certificate>();

            foreach (var application in applications)
            {
                var durationIntershipDays = 0;
                var durationCourseDays = 0;
                foreach (var qualification in application.Qualifications)
                {
                    if (qualification.Type == QualificationType.Intership)
                    {
                        durationIntershipDays += qualification.DurationDays;
                    }

                    if (qualification.Type == QualificationType.QualificationCourse)
                    {
                        durationCourseDays += qualification.DurationDays;
                    }
                }
                var certificate = new Certificate()
                {
                    Id = application.Id,
                    FirstName = application.FirstName,
                    MiddleName = application.MiddleName,
                    LastName = application.LastName,

                    Municipality = application.Address.Municipality,
                    Region = application.Address.Region,
                    City = application.Address.City,

                    DurationCourseDays = durationCourseDays,
                    DurationIntershipDays = durationIntershipDays,
                    Status = Enum.GetName(typeof(ApplicationStatus), application.Status)

                };
                certificates.Add(certificate);
            }
            

            return certificates;
        }
    }
}
