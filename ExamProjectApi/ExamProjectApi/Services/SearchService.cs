﻿using ExamProjectApi.Interfaces;
using ExamProjectApi.Models;
using ExamProjectApi.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamProjectApi.Services
{
    public class SearchService : ISearchService
    {
        private readonly ProjectDbContext context;
        public SearchService(ProjectDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Application>> SearchApplication(SearchDto searchDto)
        {
            var applications = context.Applications
                .Include(ap => ap.Qualifications)
                .Include(ap => ap.Address)
                .AsQueryable();

            if (!string.IsNullOrEmpty(searchDto.FirstName))
            {
                applications = applications.Where(ap => ap.FirstName.ToLower().Contains(searchDto.FirstName.ToLower()));
            }

            if(!string.IsNullOrEmpty(searchDto.MiddleName))
            {
                applications = applications.Where(ap => ap.MiddleName.ToLower().Contains(searchDto.MiddleName.ToLower()));
            }

            if(!string.IsNullOrEmpty(searchDto.LastName))
            {
                applications = applications.Where(ap => ap.LastName.ToLower().Contains(searchDto.LastName.ToLower()));
            }

            if (!string.IsNullOrEmpty(searchDto.City))
            {
                applications = applications.Where(ap => ap.Address.City.ToLower().Contains(searchDto.City.ToLower()));
            }

            if (!string.IsNullOrEmpty(searchDto.Municipality))
            {
                applications = applications.Where(ap => ap.Address.Municipality.ToLower().Contains(searchDto.Municipality.ToLower()));
            }

            if (!string.IsNullOrEmpty(searchDto.Region))
            {
                applications = applications.Where(ap => ap.Address.Region.ToLower().Contains(searchDto.Region.ToLower()));
            }

            if (!string.IsNullOrEmpty(searchDto.Status.ToString()) && searchDto.Status != -1)
            {
                applications = applications.Where(ap => ((int)ap.Status) == searchDto.Status);
            }

            return await applications.ToListAsync();
        }
    }
}
