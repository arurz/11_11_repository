﻿using ExamProjectApi.Interfaces;
using ExamProjectApi.Models;
using ExamProjectApi.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamProjectApi.Services
{
    public class AdminService : IAdminService
    {
        private readonly ProjectDbContext context;
        private readonly EmailService emailService;

        public AdminService(ProjectDbContext context, EmailService emailService)
        {
            this.context = context;
            this.emailService = emailService;
        }

        public async Task ApproveApplication(Application application)
        {
            application.Status = Enums.ApplicationStatus.Approved;
            await emailService.SendEmail(application);
            foreach (var qualification in application.Qualifications)
            {
                qualification.DurationDays = (qualification.FinalDate - qualification.StartDate).Days;

                qualification.StartDate.ToUniversalTime();
                qualification.FinalDate.ToUniversalTime();
            }

            context.Applications.Update(application);
            await context.SaveChangesAsync();
        }

        public async Task DisApproveApplication(Application application)
        {
            application.Status = Enums.ApplicationStatus.Disapproved;
            await emailService.SendEmail(application);
            foreach (var qualification in application.Qualifications)
            {
                qualification.DurationDays = (qualification.FinalDate - qualification.StartDate).Days;

                qualification.StartDate.ToUniversalTime();
                qualification.FinalDate.ToUniversalTime();
            }

            context.Applications.Update(application);
            await context.SaveChangesAsync();
        }

        public async Task<List<ApplicationDto>> GetApplications()
        {
            var models = new List<ApplicationDto>();
            foreach(var app in await context.Applications.ToListAsync())
            {
                var dto = new ApplicationDto()
                {
                    Id = app.Id,
                    FirstName = app.FirstName,
                    MiddleName = app.MiddleName,
                    LastName = app.LastName
                };
                models.Add(dto);
            }
            return models;
        }
    }
}

