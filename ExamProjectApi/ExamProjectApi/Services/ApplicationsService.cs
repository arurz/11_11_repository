﻿using ExamProjectApi.Interfaces;
using ExamProjectApi.Models;
using ExamProjectApi.Persistence;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace ExamProjectApi.Services
{
    public class ApplicationsService : ICheckedApplicationsService, IDraftApplicationService, IApplicationService
    {
        private readonly ProjectDbContext context;
        private readonly EmailService emailService;
        public ApplicationsService(ProjectDbContext context, EmailService emailService)
        {
            this.context = context;
            this.emailService = emailService;
        }

        public async Task<Application> GetApplicationById(int id)
        {
            var application = await context.Applications
                .Include(ap => ap.Address)
                .Include(ap => ap.Qualifications)
                .SingleOrDefaultAsync(ap => ap.Id == id);
            return application;
        }

        public async Task UpdateApplication(Application application)
        {
            context.Applications.Update(application);
            await context.SaveChangesAsync();
        }

        public async Task StoreCheckedApplication(Application application)
        {
            application.Status = Enums.ApplicationStatus.InProcessing;
            foreach(var qualification in application.Qualifications)
            {
                qualification.DurationDays = (qualification.FinalDate - qualification.StartDate).Days;

                qualification.StartDate.ToUniversalTime();
                qualification.FinalDate.ToUniversalTime();
            }

            await emailService.SendEmail(application);

            context.Applications.Update(application);
            await context.SaveChangesAsync();
        }

        public async Task StoreDraftApplication(Application application)
        {
            application.Status = Enums.ApplicationStatus.InCreation;
            foreach(var qualification in application.Qualifications)
            {
                qualification.DurationDays = (qualification.FinalDate - qualification.StartDate).Days;

                qualification.StartDate.ToUniversalTime();
                qualification.FinalDate.ToUniversalTime();
            }

            await emailService.SendEmail(application);

            await context.Applications.AddAsync(application);
            await context.SaveChangesAsync();
        }
    }
}
