﻿using ExamProjectApi.Interfaces;
using ExamProjectApi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ExamProjectApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ApplicationsController : ControllerBase
    {
        private readonly ICheckedApplicationsService checkedApplicationsService;
        private readonly IDraftApplicationService draftApplicationService;
        private readonly IApplicationService applicationService;
        public ApplicationsController(ICheckedApplicationsService checkedApplicationsService, IDraftApplicationService draftApplicationService, IApplicationService applicationService)
        {
            this.checkedApplicationsService = checkedApplicationsService;
            this.draftApplicationService = draftApplicationService;
            this.applicationService = applicationService;
        }

        [HttpGet("{id}")]
        public async Task<Application> GetApplicationById([FromRoute] int id) => await applicationService.GetApplicationById(id);

        [HttpPut("checked")]
        public async Task StoreCheckedApplication([FromBody] Application application) => await checkedApplicationsService.StoreCheckedApplication(application);

        [HttpPost("draft")]
        public async Task StoreDraftApplication([FromBody] Application application) => await draftApplicationService.StoreDraftApplication(application);

        [HttpPut("modify")]
        public async Task UpdateApplication([FromBody] Application application) => await applicationService.UpdateApplication(application);
    }
}
