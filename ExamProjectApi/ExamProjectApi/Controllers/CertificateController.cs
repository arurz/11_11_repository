﻿using ExamProjectApi.Interfaces;
using ExamProjectApi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ExamProjectApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CertificateController : ControllerBase
    {
        private readonly ICertificateService certificateService;
        public CertificateController(ICertificateService certificateService)
        {
            this.certificateService = certificateService;
        }

        [HttpGet("{id}")]
        public async Task<Certificate> GetCertificateById([FromRoute] int id) => await certificateService.GetCertificateById(id);
    }
}
