﻿using ExamProjectApi.Interfaces;
using ExamProjectApi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamProjectApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService adminService;
        public AdminController(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        [HttpPut("approve")]
        public async Task ApproveApplication([FromBody] Application application) => await adminService.ApproveApplication(application);

        [HttpPut("disapprove")]
        public async Task DisApproveApplication([FromBody] Application application) => await adminService.DisApproveApplication(application);

        [HttpGet("applications")]
        public async Task<List<ApplicationDto>> GetApplications() => await adminService.GetApplications();
    }
}

