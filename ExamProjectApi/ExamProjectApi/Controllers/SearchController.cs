﻿using ExamProjectApi.Interfaces;
using ExamProjectApi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamProjectApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly ISearchService searchService;
        private readonly ICertificateService certificateService;
        public SearchController(ICertificateService certificateService, ISearchService searchService)
        {
            this.searchService = searchService;
            this.certificateService = certificateService;
        }

        [HttpGet("applications")]
        public async Task<List<Certificate>> SearchApplications([FromQuery] SearchDto searchDto)
        {
            var applications = await searchService.SearchApplication(searchDto);
            var certificates = await certificateService.GetCertificates(applications);
            return certificates;
        }
    }
}
