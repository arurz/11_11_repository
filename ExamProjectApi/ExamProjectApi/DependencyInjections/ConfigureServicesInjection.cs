﻿using ExamProjectApi.Interfaces;
using ExamProjectApi.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ExamProjectApi.DependencyInjections
{
    public static class ConfigureServicesInjection
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IApplicationService, ApplicationsService>();
            services.AddScoped<ICheckedApplicationsService, ApplicationsService>();
            services.AddScoped<IDraftApplicationService, ApplicationsService>();
            services.AddScoped<ICertificateService, CertificateService>();

            services.AddScoped<ISearchService, SearchService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<EmailService>();

            return services;
        }
    }
}
