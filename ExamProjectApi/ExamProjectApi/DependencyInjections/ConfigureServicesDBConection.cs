﻿using ExamProjectApi.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ExamProjectApi.DependencyInjections
{
    public static class ConfigureServicesDBConection
    {
        public static IServiceCollection AddDBConnection(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ProjectDbContext>(
                options => options.UseNpgsql(configuration["DBConnection:ConnectionString"]));

            return services;
        }
    }
}
