﻿using ExamProjectApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamProjectApi.Interfaces
{
    public interface IAdminService
    {
        Task ApproveApplication(Application application);
        Task DisApproveApplication(Application application);

        Task<List<ApplicationDto>> GetApplications();
    }
}
