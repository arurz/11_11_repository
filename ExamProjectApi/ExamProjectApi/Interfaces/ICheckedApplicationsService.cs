﻿using ExamProjectApi.Models;
using System.Threading.Tasks;

namespace ExamProjectApi.Interfaces
{
    public interface ICheckedApplicationsService
    {
        Task StoreCheckedApplication(Application application);
    }
}
