﻿using ExamProjectApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamProjectApi.Interfaces
{
    public interface ISearchService
    {
        public Task<List<Application>> SearchApplication(SearchDto searchDto);
    }
}
