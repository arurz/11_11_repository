﻿using ExamProjectApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamProjectApi.Interfaces
{
    public interface ICertificateService
    {
        Task<List<Certificate>> GetCertificates(List<Application> applications);

        Task<Certificate> GetCertificateById(int id);
    }
}
