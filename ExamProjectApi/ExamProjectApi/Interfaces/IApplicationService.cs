﻿using ExamProjectApi.Models;
using System.Threading.Tasks;

namespace ExamProjectApi.Interfaces
{
    public interface IApplicationService
    {
        Task<Application> GetApplicationById(int id);
        Task UpdateApplication(Application application);
    }
}
