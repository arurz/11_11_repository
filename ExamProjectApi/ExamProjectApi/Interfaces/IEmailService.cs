﻿using ExamProjectApi.Models;
using System.Threading.Tasks;

namespace ExamProjectApi.Interfaces
{
    public interface IEmailService
    {
        Task SendEmail(Application application);
    }
}
