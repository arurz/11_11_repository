﻿using ExamProjectApi.Models;
using System.Threading.Tasks;

namespace ExamProjectApi.Interfaces
{
    public interface IDraftApplicationService
    {
        Task StoreDraftApplication(Application application);
    }
}
