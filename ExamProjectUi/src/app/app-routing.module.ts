import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { ApplicationSenderComponent } from './components/application-sender/application-sender.component';
import { CertificateComponent } from './components/certificate/certificate.component';
import { ChangeApplicationComponent } from './components/change-application/change-application.component';
import { SearchComponent } from './components/search/search.component';

const routes: Routes = [
  { path: 'applications/form', component: ApplicationSenderComponent },
  { path: 'search', component: SearchComponent },
  { path: 'application/modify/:id', component: ChangeApplicationComponent },
  { path: 'admin', component: AdminPageComponent },
  { path: 'certificate/:id', component: CertificateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
