import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Application } from '../models/application.model';
import { ChangeDto } from '../models/change-dto.model';

@Injectable({
  providedIn: 'root'
})
export class ModifyApplicationService {

  readonly url = 'api/applications';
  constructor(private http: HttpClient) { }

  modifyApplications(form: Application): Observable<any> {
    return this.http.put<any>(this.url + '/modify', form);
  }

  getApplicationById(id: number): Observable<Application> {
    return this.http.get<Application>(`${this.url}/${id}`);
  }

  approveApplication(form: Application): Observable<any> {
    return this.http.put<any>("api/admin/approve", form);
  }

  disApproveApplication(form: Application): Observable<any> {
    return this.http.put<any>("api/admin/disapprove", form);
  }

  submitApplication(form: Application): Observable<any>{
    return this.http.put<any>(this.url + '/checked', form);
  }
}
