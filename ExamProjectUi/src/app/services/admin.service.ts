import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApplicationDto } from '../models/application-dto.model';
import { Application } from '../models/application.model';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  readonly url = 'api/admin';
  constructor(private http: HttpClient) { }

  getApplications(): Observable<ApplicationDto[]> {
    return this.http.get<ApplicationDto[]>(`${this.url}/applications`);
  }
}
