import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Certificate } from '../models/certificate.model';

@Injectable({
  providedIn: 'root'
})
export class CertificateService {

  readonly url = "api/certificate"
  constructor(private http: HttpClient) { }

  getCertificate(id: number): Observable<Certificate>{
    return this.http.get<Certificate>(`${this.url}/${id}`)
  }
}
