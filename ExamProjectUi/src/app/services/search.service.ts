import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Application } from '../models/application.model';
import { Certificate } from '../models/certificate.model';
import { SearchDto } from '../models/search-dto.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  readonly url = "api/search/applications";

  constructor(private http: HttpClient) { }

  private composeQueryString(object: any): string {
    let result = '';
    let isFirst = true;
    if (object) {
      Object.keys(object)
        .filter(key => object[key] !== null && object[key] !== undefined)
        .forEach(key => {
          let value = object[key];
          if (value instanceof Date) {
            value = value.toISOString();
          }
          if (isFirst) {
            result = '?' + key + '=' + value;
            isFirst = false;
          } else {
            result += '&' + key + '=' + value;
          }
        });
    }
    return result;
  }

  searchApplication(searchDto: SearchDto): Observable<Certificate[]> {
    let query = this.url + this.composeQueryString(searchDto);
    return this.http.get<Certificate[]>(query);
  }
}
