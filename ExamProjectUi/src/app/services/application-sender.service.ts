import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Application } from '../models/application.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApplicationSenderService {

  readonly url = "/api/applications"
  constructor(private http: HttpClient) { }

  storeDraftApplication(form: Application): Observable<any> {
    return this.http.post<any>(this.url + '/draft', form);
  }
}
