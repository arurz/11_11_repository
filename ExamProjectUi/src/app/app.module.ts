import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApplicationSenderComponent } from './components/application-sender/application-sender.component';
import { SearchComponent } from './components/search/search.component';
import { MenuComponent } from './components/menu/menu.component';
import { ChangeApplicationComponent } from './components/change-application/change-application.component';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule} from "ngx-spinner";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CertificateComponent } from './components/certificate/certificate.component'

@NgModule({
  declarations: [
    AppComponent,
    ApplicationSenderComponent,
    SearchComponent,
    MenuComponent,
    ChangeApplicationComponent,
    AdminPageComponent,
    CertificateComponent,
    
  ],
  imports: [
    HttpClientModule,
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    BrowserAnimationsModule
  ],
  providers: [
    HttpClientModule,
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
