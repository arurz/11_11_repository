import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Application } from 'src/app/models/application.model';
import { Certificate } from 'src/app/models/certificate.model';
import { SearchDto } from 'src/app/models/search-dto.model';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchDto: SearchDto = new SearchDto();
  status: number = -1;
  applications: Application[] = [];
  certificates: Certificate[] = [];

  p: number = 1;
  count: number = 7;  

  constructor(private searchService: SearchService,
    private spinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  searchApplications() {
    this.spinnerService.show();
    this.searchDto.status = this.status;
    this.searchService.searchApplication(this.searchDto)
      .subscribe( (certificates: any) => {
        this.certificates = certificates;
        this.spinnerService.hide();
      });
  }

  deleteFilter(){
    this.searchDto = new SearchDto();
    this.status = -1;
  }
}
