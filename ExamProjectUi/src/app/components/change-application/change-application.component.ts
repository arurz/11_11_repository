import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Application } from 'src/app/models/application.model';
import { ModifyApplicationService } from 'src/app/services/modify-application.service';

@Component({
  selector: 'app-change-application',
  templateUrl: './change-application.component.html',
  styleUrls: ['./change-application.component.css']
})
export class ChangeApplicationComponent implements OnInit {

  id = Number(this.route.snapshot.paramMap.get('id'));
  applicationForm: Application = new Application();
  InvalidDate: string;

  constructor(private route: ActivatedRoute,
    private router: Router, 
    private modifyApplicationService: ModifyApplicationService,
    private spinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getApplicationById();
  }

  getApplicationById() {
    this.spinnerService.show();
    this.modifyApplicationService.getApplicationById(this.id)
      .subscribe( (applicationForm: Application) => {
        this.applicationForm = applicationForm;
        this.removeDates();
        this.spinnerService.hide();
      });
  }

  removeDates(){
    for(let i = 0; i < this.applicationForm.qualifications.length; i++){
      this.applicationForm.qualifications[i].startDateString =  this.applicationForm.qualifications[i].startDate.toString();
      this.applicationForm.qualifications[i].startDate = new Date(Date.parse(this.InvalidDate));

      this.applicationForm.qualifications[i].finalDateString =  this.applicationForm.qualifications[i].finalDate.toString();
      this.applicationForm.qualifications[i].finalDate = new Date(Date.parse(this.InvalidDate));
    }
  }

  storeDraftApplications() {
    this.setDefaultTime();
    this.modifyApplicationService.modifyApplications(this.applicationForm)
      .subscribe(() => this.router.navigateByUrl("/search"));
  }

  submitApplication() {
    this.setDefaultTime();
    this.modifyApplicationService.submitApplication(this.applicationForm)
      .subscribe(() => this.router.navigateByUrl("/search"));
  }

  approveApplication() {
    this.setDefaultTime();
    this.modifyApplicationService.approveApplication(this.applicationForm)
      .subscribe(() => this.router.navigateByUrl("/search"));
  }

  disApproveApplication() {
    this.setDefaultTime();
    this.modifyApplicationService.disApproveApplication(this.applicationForm)
      .subscribe(() => this.router.navigateByUrl("/search"));
  }

  setDefaultTime(){
    for(let i = 0; i < this.applicationForm.qualifications.length; i++)
    {
      let stringStartDate: string = this.applicationForm.qualifications[i].startDate.toString();
      let newStartDate = new Date(stringStartDate += "T00:00:00");
      this.applicationForm.qualifications[i].startDate = newStartDate;

      let stringFinalDate: string = this.applicationForm.qualifications[i].finalDate.toString();
      let newFinaltDate = new Date(stringFinalDate += "T00:00:00");
      this.applicationForm.qualifications[i].finalDate = newFinaltDate;
    }
  }
}
