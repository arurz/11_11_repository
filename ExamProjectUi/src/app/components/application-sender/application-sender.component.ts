import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Address } from 'src/app/models/address.model';
import { Application } from 'src/app/models/application.model';
import { Qualification } from 'src/app/models/qualification.model';
import { ApplicationSenderService } from 'src/app/services/application-sender.service';


@Component({
  selector: 'app-application-sender',
  templateUrl: './application-sender.component.html',
  styleUrls: ['./application-sender.component.css']
})
export class ApplicationSenderComponent implements OnInit {
  
  requestType: number = 0;
  qualifications: Qualification[] = [new Qualification()];
  applicationForm: Application = new Application();
  address: Address = new Address();

  constructor(private applicationSenderService: ApplicationSenderService,
    private router: Router) { }

  ngOnInit(): void {
  }

  storeDraftApplications() {
    this.applicationForm.qualifications = this.qualifications;
    this.applicationForm.address = this.address;
    this.applicationForm.requestType = this.requestType;

    for(let i = 0; i < this.applicationForm.qualifications.length; i++)
    {
      let stringStartDate: string = this.applicationForm.qualifications[i].startDate.toString();
      let newStartDate = new Date(stringStartDate += "T00:00:00");
      this.applicationForm.qualifications[i].startDate = newStartDate;

      let stringFinalDate: string = this.applicationForm.qualifications[i].finalDate.toString();
      let newFinaltDate = new Date(stringFinalDate += "T00:00:00");
      this.applicationForm.qualifications[i].finalDate = newFinaltDate;
    }

    this.applicationSenderService.storeDraftApplication(this.applicationForm)
      .subscribe(() => this.router.navigateByUrl("/search"));
  }
  
  addQualification(){
    let tempQualification: Qualification = new Qualification();
    this.qualifications.push(tempQualification);
  }

  removeQualification(id: number){
    if(this.qualifications.length > 1)
      this.qualifications.splice(id, 1);
    else{
      this.qualifications = [new Qualification()];
    }
  }
}
