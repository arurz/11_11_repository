import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApplicationDto } from 'src/app/models/application-dto.model';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  applications: ApplicationDto[] = [];
  p: number = 1;
  count: number = 7;  
  constructor(private adminService: AdminService,
    private spinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getApplications();
  }

  getApplications() {
    this.spinnerService.show();
    this.adminService.getApplications()
      .subscribe( (applications: ApplicationDto[]) => {
        this.applications = applications;
        this.spinnerService.hide();
      });
  }

}
