import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Address } from 'src/app/models/address.model';
import { Certificate } from 'src/app/models/certificate.model';
import { CertificateService } from 'src/app/services/certificate.service';

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.css']
})
export class CertificateComponent implements OnInit {

  certificate: Certificate = new Certificate();
  constructor(private certificateService: CertificateService,
    private route: ActivatedRoute,
    private spinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getCertificate();
  }

  getCertificate(){
    this.spinnerService.show();
    let id = Number(this.route.snapshot.paramMap.get('id'));
    this.certificateService.getCertificate(id)
      .subscribe((certificate: Certificate) => {
        this.certificate = certificate;
        this.spinnerService.hide();
      })
  }
}
