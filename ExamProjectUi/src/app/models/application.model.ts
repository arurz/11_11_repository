import { Address } from "./address.model";
import { Qualification } from "./qualification.model";

export class Application {
  id: number;
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  requestType: number;

  address: Address;

  qualifications: Qualification[] = [];
}
