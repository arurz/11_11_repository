export class ApplicationDto {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
}