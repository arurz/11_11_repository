export class ChangeDto {
  firstName: string;
  middleName: string;
  lastName: string;
}
