export class Qualification {
  id: number;
  startDate: Date;
  startDateString: string;
  finalDate: Date;
  finalDateString: string;
  durationDays: number;
  shortDescription: string;
  type: number;
}