export class SearchDto {
  firstName: string;
  middleName: string;
  lastName: string;
  municipality: string;
  region: string;
  city: string;

  status: number;
}
