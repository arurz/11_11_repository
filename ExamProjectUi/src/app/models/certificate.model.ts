export class Certificate {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    
    municipality: string;
    region: string;
    city: string;

    durationIntershipDays: number;
    durationCourseDays: number;

    status: number;
}
