export class Address {
  id: number;
  municipality: string;
  region: string;
  city: string;
  applicantAddress: string;
}
